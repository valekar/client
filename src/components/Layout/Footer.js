import React from 'react';

import { Navbar, Nav, NavItem } from 'reactstrap';

const Footer = () => {
  return (
    <Navbar>
      <Nav navbar>
        <NavItem>2018 Valekar company</NavItem>
      </Nav>
    </Navbar>
  );
};

export default Footer;
