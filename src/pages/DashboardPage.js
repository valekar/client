import Page from 'components/Page';

import React from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Button,
} from 'reactstrap';
import { connect } from 'react-redux';
import { deleteApplique } from '../store/actions/appliqueActions';

const today = new Date();

class DashboardPage extends React.Component {
  componentDidMount() {
    // this is needed, because InfiniteCalendar forces window scroll
    window.scrollTo(0, 0);
  }

  handleDelete = e => {
    this.props.deleteApplique(1);
  };

  render() {
    console.log(this.props);

    return (
      <Page
        className="DashboardPage"
        title="Dashboard"
        breadcrumbs={[{ name: 'Dashboard', active: true }]}
      >
        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Appliques</CardHeader>
              <Table>
                <thead>
                  <tr>
                    <th scope="col">Type</th>
                    <th scope="col">Column heading</th>
                    <th scope="col">Column heading</th>
                    <th scope="col">Column heading</th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="table-active">
                    <th scope="row">Active</th>
                    <td>Column content</td>
                    <td>Column content</td>
                    <td>Column content</td>
                  </tr>
                </tbody>
              </Table>
              <CardBody />
            </Card>
          </Col>
        </Row>

        <Button color="primary" onClick={this.handleDelete}>
          Delete Applique
        </Button>
      </Page>
    );
  }
}

const mapStateToProps = state => {
  return {
    appliques: state.applique.appliques,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteApplique: id => {
      dispatch(deleteApplique(id));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DashboardPage);
