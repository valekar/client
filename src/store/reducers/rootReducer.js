import appliqueReducer from './appliqueReducer';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  applique: appliqueReducer,
});

export default rootReducer;
