export const createApplique = applique => {
  return (dispatch, getState) => {
    // make async call to database

    dispatch({ type: 'CREATE_APPLIQUE', applique });
  };
};

export const deleteApplique = applique => {
  return (dispatch, getState) => {
    console.log('Make aync call');
    dispatch({ type: 'DELETE_APPLIQUE', applique });
  };
};
